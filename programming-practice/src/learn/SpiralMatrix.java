package learn;

public class SpiralMatrix {
	 static int[][] matrix={{1,2, 3, 4,5},{5, 6, 7, 8,9},{9, 10, 11, 12,13},{13, 14, 15,16,17}};
	 
	static void printSpiral(int[][] mat,int m,int n)
	 {
		int top=0,bot=m-1,left=0,right=n-1;
		while(top<bot && right>left)
		{
			
			
			for(int i=left ;i<=right;i++)
			{
				System.out.print(mat[top][i] +" ");
			}
			top++;
			for(int i=top ;i<=bot;i++)
			{
				System.out.print(mat[i][right]+" ");
			}
			right--;
			for (int i=right; i>=left;i--)
			{
				System.out.print(mat[bot][i]+" ");
			}
			bot--;
			for (int i=bot;i>=top;i--)
			{
				System.out.print(mat[i][left]+" ");
			}
			left++;
		}
	 }
	 
	public static void main(String[] args) {
		printSpiral(matrix, 4, 5);
		
	}
	 
	           		 
}
