package learn;

import java.util.Arrays;

public class RotateArray {
  
	
	public static void main(String[] args) {
		int[] arr=new int[]{1,2,3,4,5,6,7,8,9,8,4,5,6,4,3,34,3,4,3,45,5,6,6,7};
		int rotateBy=5;
		rotateArray(arr,rotateBy);
	}

	private static void rotateArray(int[] arr, int d) {		
	//	int[]  newArr= new int[d];
	//	Arrays.stream(newArr).forEach(action);
	/*	for(int i=0;i<d;i++)
		{
			newArr[i]=arr[i];	
			
		}*/
		//printArray(newArr);
		
		for(int i=d,j=arr.length-d;i<arr.length&& j<arr.length;j++, i++)
		{
			arr[j]=arr[i-d];
			arr[i-d]=arr[i];
		
		}
		//printArray(arr);
	/*	for(int i=arr.length-d, j=0;i<arr.length && j<d;i++,j++)
		{
			
			arr[i]=newArr[j];	
			
		}*/
		
		printArray(arr);
	}

	public static void printArray(int[] arr) {		
		Arrays.stream(arr).forEach(a->System.out.print(a+" "));
		System.out.println("\n");
	}
}
