package learn;

public class SingeltonDemo {

	private static volatile SingeltonDemo instance;
	
	private SingeltonDemo()
	{
		
	}
	
	public SingeltonDemo getInstance()
	{
		if (instance==null)
			synchronized(this) {
				if(instance==null)
					instance= new SingeltonDemo();
			}
		return instance;
	}
}
