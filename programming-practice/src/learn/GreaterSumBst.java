package learn;

/**
 * 
 * this program converts BST into a tree in which each node data is replaced by
 * sum of all the nodes greater than its data.
 * 
 * @author Jhakkas
 *
 */

public class GreaterSumBst {

	public static void main(String[] args) {

		BST tree = new BST();
		tree.root = new Node(10);
		tree.root.left = new Node(8);
		tree.root.right = new Node(11);
		tree.printInorderTraversal(tree.root);

		Node treeNode = tree.doOperation(tree.root);
		System.out.println(" ");
		tree.printInorderTraversal(treeNode);

	}

}

class Node {
	int data;
	Node left, right;

	Node(int data) {
		this.data = data;
		left = right = null;
	}
}

class Sum{
	int sum=0;
}
class BST {
	Node root;
	Sum sum = new Sum();;

	Node doOperation(Node root) {
		ConvertIntoGreaterSum(root, sum);
		return root;
	}

	void ConvertIntoGreaterSum(Node node, Sum sum2) {
		if (node == null)
			return;
		ConvertIntoGreaterSum(node.right, sum2);
		sum2.sum = sum2.sum + node.data;
		node.data = sum2.sum;
		ConvertIntoGreaterSum(node.left, sum2);

	}

	void printInorderTraversal(Node root) {
		if (root == null)
			return;
		printInorderTraversal(root.left);
		System.out.print(root.data + " ");
		printInorderTraversal(root.right);

	}
}
